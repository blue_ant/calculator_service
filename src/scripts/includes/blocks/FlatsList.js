class FlatsList {
    constructor(opts = {}) {
        let tpl;
        if (opts.tpl) {

            tpl = opts.tpl;
        } else {
            tpl = `
    /*=require ../chunks/FlatsList.tpl*/
`;
        }
        this.compiledTemplate = _.template(tpl.replace(/\s{2,}/g, ""));

        let $cont = $(opts.mountEl);
        this.mountEl = $cont.get(0);

        $cont.on('click', '.FlatsList_showMoreBtn', (event) => {
            event.preventDefault();
            console.log("Button clicked!!!");
        });


    }

    render(data) {
        this.mountEl.innerHTML = this.compiledTemplate(data);
    }
}