class MortgageCalc {
	constructor(formEl, opts) {
		this.$form = $(formEl);

		if (!this.$form.length) {
			console.error('MortgageCalc constructor can\'t find given "formEl" in DOM!');
			return;
		}

		this.$form.on("submit", function (event) {
			event.preventDefault();
			return false;
		});

		this.banksListBuilder = opts.banksListBuilder;
		this.flatListBuilder = opts.flatListBuilder;

		this._onFormElementsChange = _.debounce((event) => {

			// ignore sliders events fired throught JS-API
			if (event.target.parentNode.classList.contains("MortgageSlider") && !event.originalEvent) return false;
			this.getAndRenderBanks();
		}, 100);

		this.$creditSummCount = this.$form.find(".MortgageCalc_summVal");

		this.$bldSelector = this.$form.find(".MortgageSelect_select").on('change', this._onFormElementsChange).fSelect({
			placeholder: "Все объекты",
			numDisplayed: 2,
			overflowText: '{n} выбрано',
		});

		this.initSliders();
	}

	getAndRenderBanks() {
		$.ajax({
				url: this.$form.attr("action"),
				type: this.$form.attr("method"),
				dataType: "json",
				data: this.$form.serializeObject(),
			})
			.done((response) => {
				console.log(response);
				this.banksListBuilder.render({ banks: response.banks });
				this.flatListBuilder.render({ flats: response.flats });
				$('[data-credit]').html(response.credit.toLocaleString("RU-ru") + ' руб.');
			})
			.fail(function () {
				alert("Не удалось получить данные с сервера. Попробуйте позже или обратитесь к админстрации сайта.");
			})
			.always(function () {
				// console.info("complete");
			});
	}

	initSliders() {
		throw new TypeError("Cannot construct Abstract instances directly");
	}
}

class MortgagePaymentCalc extends MortgageCalc {
	initSliders() {
		{
			let $formSliderBlock = this.$form.find(".MortgageCalc_cell-firstPayment");

			var firstPaymentSliderInst = new MortgageSlider(
				$formSliderBlock.find(".MortgageSlider"), {
					change: this._onFormElementsChange,
				}, {
					slideEventCallback: ( /*event, ui*/ ) => {
						let selectedFirstPayment = firstPaymentSliderInst.getValue();
						let selectedPrice = priceSliderInst.getValue();

						if (selectedFirstPayment >= selectedPrice * 0.9) {
							priceSliderInst.setHandlerPosition(Math.round(selectedFirstPayment * 1.15));
						}
					},
					$counter: $formSliderBlock.find(".MortgageCalc_sum"),
				}
			);
		}

		{
			let $formSliderBlock = this.$form.find(".MortgageCalc_cell-price");

			var priceSliderInst = new MortgageSlider(
				$formSliderBlock.find(".MortgageSlider"), {
					change: this._onFormElementsChange,
				}, {
					slideEventCallback: ( /*event, ui*/ ) => {
						let selectedFirstPayment = firstPaymentSliderInst.getValue();
						let selectedPrice = priceSliderInst.getValue();

						if (selectedFirstPayment >= selectedPrice * 0.9) {
							firstPaymentSliderInst.setHandlerPosition(Math.round(selectedPrice * 0.9));
						}
					},
					$counter: $formSliderBlock.find(".MortgageCalc_sum"),
				}
			);
		}

		{
			let $formSliderBlock = this.$form.find(".MortgageCalc_cell-duration");

			new MortgageSlider(
				$formSliderBlock.find(".MortgageSlider"), {
					change: this._onFormElementsChange,
				}, {
					$counter: $formSliderBlock.find(".MortgageCalc_sum"),
					customCounterRender: (v) => {
						if (v === 1) {
							return "1 год";
						} else {
							return moment.duration(v, "years").humanize();
						}
					},
				}
			);
		}
	}
}

class MortgageSummCalc extends MortgageCalc {
	initSliders() {
		{
			let $formSliderBlock = this.$form.find(".MortgageCalc_cell-monthlyPayment");

			var monthlyPaymentSliderInst = new MortgageSlider(
				$formSliderBlock.find(".MortgageSlider"), {
					change: this._onFormElementsChange,
				}, {
					$counter: $formSliderBlock.find(".MortgageCalc_sum"),
				}
			);
		}

		{
			let $formSliderBlock = this.$form.find(".MortgageCalc_cell-firstPayment");

			var firstPaymentSliderInst = new MortgageSlider(
				$formSliderBlock.find(".MortgageSlider"), {
					change: this._onFormElementsChange,
				}, {
					$counter: $formSliderBlock.find(".MortgageCalc_sum"),
				}
			);
		}

		{
			let $formSliderBlock = this.$form.find(".MortgageCalc_cell-duration");

			new MortgageSlider(
				$formSliderBlock.find(".MortgageSlider"), {
					change: this._onFormElementsChange,
				}, {
					$counter: $formSliderBlock.find(".MortgageCalc_sum"),
					customCounterRender: (v) => {
						if (v === 1) {
							return "1 год";
						} else {
							return moment.duration(v, "years").humanize();
						}
					},
				}
			);
		}
	}
}
