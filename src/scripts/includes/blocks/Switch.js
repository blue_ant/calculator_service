class Switch {
    constructor(el, opts = {}) {
        this.$el = $(el).eq(0);
        this.$checkbox = this.$el.find(".Switch_checkbox");
        this.$labels = this.$el.find(".Switch_label");
        this.$labels.on("click", (event) => {
            event.preventDefault();
            this.$checkbox.trigger("click");
        });
        this.$checkbox.on("change", (event) => {
            this.$labels.toggleClass("Switch_label-active");
            if (opts.onChange) {
                let isChecked = this.$checkbox.is(":checked");
                opts.onChange(isChecked, this.$el);
            }
        });
    }

    // methods
}