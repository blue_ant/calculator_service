class MortgageSlider {
    constructor(el, opts, addOpts) {
        let $el = $(el);

        if (!$el.length) {
            console.error('MortgageSlider constructor can\'t find given "el" in DOM!');
            return;
        }

        this.customCounterRender = addOpts.customCounterRender ? addOpts.customCounterRender : false;
        this.$inp = $el.find("input");

        let $counter = addOpts.$counter;
        let minAllowedVal = parseInt(this.$inp.data("min-value"));
        let maxAllowedVal = parseInt(this.$inp.data("max-value"));
        let options = {
            classes: {
                "ui-slider-handle": "MortgageSlider_handle",
                "ui-slider-range": "MortgageSlider_progress",
            },
            min: minAllowedVal,
            max: maxAllowedVal,
            range: "min",
            value: this.$inp.val(),
            create: (event /*, ui*/) => {
                this._setValue(this.$inp.val());

                $(event.target)
                    .find(".MortgageSlider_handle")
                    .insertBefore($(event.target).find(".MortgageSlider_progress"));
            },
            slide: (event, ui) => {
                this._setValue(ui.value);
                if (addOpts.slideEventCallback) addOpts.slideEventCallback();
            },
        };

        if (opts) {
            for (let key in opts) {
                options[key] = opts[key];
            }
        }

        let $track = $("<div class='MortgageSlider_track'></div>");
        $track.slider(options).appendTo($el);

        $counter.on("click", (event) => {
            if (event.currentTarget.getElementsByTagName("input").length) return false;

            let $popupInp = $("<input type='text' class='MortgageSlider_popupInp'>");
            $popupInp
                .val(this.$inp.val())
                .inputmask("integer", {
                    groupSeparator: " ",
                    autoGroup: true,
                    allowMinus: false,
                    rightAlign: false,
                })
                .on("change", (event) => {
                    event.preventDefault();
                    console.log(Date.now());
                    let $tmpInp = $(event.currentTarget);
                    let val = parseInt(
                        $tmpInp
                            .val()
                            .split(" ")
                            .join("")
                    );
                    if (isNaN(val)) val = minAllowedVal;
                    this.setHandlerPosition(val);
                    if (addOpts.slideEventCallback) addOpts.slideEventCallback();
                }).on("focusout", (event) => {
                    $(event.currentTarget).trigger("change");
                });
            this.$counter.html($popupInp);
            $popupInp.trigger("focus");
        });

        this.minAllowedVal = minAllowedVal;
        this.maxAllowedVal = maxAllowedVal;
        this.$track = $track;
        this.$counter = $counter;
        this.$el = $el;
    }

    getValue() {
        return this.$inp.val();
    }

    _setValue(v) {
        v = parseInt(v);
        this.$inp.val(v);

        window.requestAnimationFrame(() => {
            if (this.customCounterRender) {
                let valToShow = this.customCounterRender(v);
                this.$counter.html(valToShow);
            } else {
                this.$counter.html(parseInt(v).toLocaleString("ru-RU"));
            }
        });
    }

    setHandlerPosition(v) {
        if (v < this.minAllowedVal) {
            v = this.minAllowedVal;
        }

        if (v > this.maxAllowedVal) {
            v = this.maxAllowedVal;
        }
        this._setValue(v);
        this.$track.slider("value", v);
    }
}
