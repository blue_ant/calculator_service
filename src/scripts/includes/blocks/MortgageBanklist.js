class MortgageBanklist {
    constructor(opts = {}) {
        let tpl = opts.tpl.replace(/\s{2,}/g, "");

        this.compiledTemplate = _.template(tpl);
        this.mountEl = $(opts.mountEl).get(0);
    }

    render(data) {
        this.mountEl.innerHTML = this.compiledTemplate(data);
    }
}
