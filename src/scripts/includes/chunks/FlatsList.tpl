<div class="FlatsList">
    <h4 class="FlatsList_title">Квартиры в рассчитанной ценовой категории</h4>
    <% _.forEach(flats, function(flatsGroup){ %>
        <h5 class="FlatsList_subtitle"><%= flatsGroup.housingName %> <span class="FlatsList_mutedText">(<%= flatsGroup.totalFound %> квартир)</span></h5>
        <div class="FlatsList_cardsWrap">
            <% _.forEach(flatsGroup.items, function(flt){ %>
                <figure class="FlatCard"><img class="FlatCard_pic" src="<%= flt.pic %>" alt="Квартира № <%= flt.num %>" title="Квартира № <%= flt.num %>">
                    <figcaption class="FlatCard_desc">
                        <h6 class="FlatCard_title">Квартира № <%= flt.num %></h6>
                        <dl class="FlatCard_details">
                            <dt class="FlatCard_detailCaption">ПЛОЩАДЬ</dt>
                            <dd class="FlatCard_detailVal"><%= flt.footage %> м²</dd>
                            <dt class="FlatCard_detailCaption">ЭТАЖ</dt>
                            <dd class="FlatCard_detailVal"><%= flt.floor %></dd>
                            <dt class="FlatCard_detailCaption">КОРП., СЕКЦИЯ</dt>
                            <dd class="FlatCard_detailVal">Корп. <%= flt.bld %>, Секц. <%= flt.section %></dd>
                        </dl>
                    </figcaption>
                    <div class="FlatCard_footer">
                        <div class="FlatCard_price"><%= flt.price.toLocaleString("RU-ru") %> ₽</div>
                        <a class="FlatCard_readMoreBtn" href="#">
                            <svg class="SvgIco SvgIco-arrowRight" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 5 14">
                                <path class="SvgIco_path" d="M4.648 7.5q0 0.102-0.078 0.18l-3.641 3.641q-0.078 0.078-0.18 0.078t-0.18-0.078l-0.391-0.391q-0.078-0.078-0.078-0.18t0.078-0.18l3.070-3.070-3.070-3.070q-0.078-0.078-0.078-0.18t0.078-0.18l0.391-0.391q0.078-0.078 0.18-0.078t0.18 0.078l3.641 3.641q0.078 0.078 0.078 0.18z"></path>
                            </svg>
                        </a>
                    </div>
                </figure>
                <%}); %>
        </div>
        <div class="FlatsList_tableWrap">
            <table class="FlatsTable">
                <thead class="FlatsTable_head">
                    <tr class="FlatsTable_headRow">
                        <th class="FlatsTable_th">ПЛАНИРОВКА</th>
                        <th class="FlatsTable_th">№</th>
                        <th class="FlatsTable_th">КОМНАТЫ</th>
                        <th class="FlatsTable_th">КОРПУС</th>
                        <th class="FlatsTable_th">СЕКЦИЯ</th>
                        <th class="FlatsTable_th">ПЛОЩАДЬ, М²</th>
                        <th class="FlatsTable_th">ЭТАЖ</th>
                        <th class="FlatsTable_th">СТОИМОСТЬ, РУБ</th>
                    </tr>
                </thead>
                <tbody class="FlatsTable_body">
                    <% _.forEach(flatsGroup.items, function(flt){ %>
                        <tr class="FlatsTable_row">
                            <td class="FlatsTable_td"><img class="FlatsTable_pic" src="<%= flt.pic %>" alt="Квартира № <%= flt.num %>" title="Квартира № <%= flt.num %>"></td>
                            <td class="FlatsTable_td"><%= flt.num %></td>
                            <td class="FlatsTable_td"><%= flt.rooms %></td>
                            <td class="FlatsTable_td"><%= flt.bld %></td>
                            <td class="FlatsTable_td">Секция <%= flt.section %></td>
                            <td class="FlatsTable_td"><%= flt.footage %> м²</td>
                            <td class="FlatsTable_td"><%= flt.floor %></td>
                            <td class="FlatsTable_td"><%= flt.price.toLocaleString("RU-ru") %> ₽</td>
                        </tr>
                    <%}); %>
                </tbody>
            </table>
        </div>
        <div class="FlatsList_footer">
            <button class="FlatsList_showMoreBtn">Показать еще 30 квартир</button>
        </div>
        <%}); %>
        
</div>