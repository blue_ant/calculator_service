<div class="MortgageBanklist">
    <div class="MortgageBanklist_caption hidden-md">Предложения от банков</div>
    <div class="MortgageBanklist_head">
        <div class="MortgageBanklist_row MortgageBanklist_row-head">
            <div class="MortgageBanklist_cell MortgageBanklist_cell-th">Банк</div>
            <div class="MortgageBanklist_cell MortgageBanklist_cell-th MortgageBanklist_cell-bankRate">
                <span class="hidden-xs">Процентная ставка</span>
                <span class="hidden-sm hidden-md">Ставка</span>
            </div>
            <div class="MortgageBanklist_cell MortgageBanklist_cell-th">
                <span class="hidden-xs">Ежемесячный платеж</span>
                <span class="hidden-sm hidden-md">Платежы</span>
            </div>
        </div>
    </div>
    <div class="MortgageBanklist_tbody">
        <% _.forEach(banks, function(bank){ %>
            <div class="MortgageBanklist_wrapper">
                <div class="MortgageBanklist_row" data-bank-toggle>
                    <div class="MortgageBanklist_cell MortgageBanklist_cell-td">
                        <div class="MortgageBanklist_logo" style="background-image:url(<%= bank.logo %>)" title="<%= bank.name %>"></div>
                    </div>
                    <div class="MortgageBanklist_cell MortgageBanklist_cell-td MortgageBanklist_cell-bankRate"> <%= bank.rate %>&nbsp;%</div>
                    <div class="MortgageBanklist_cell MortgageBanklist_cell-td">
                        <span class="MortgageBanklist_price"><%= bank.monthlyPayment.toLocaleString("RU-ru") %> руб.</span>
                        <a class="MortgageBanklist_bankLink" href="<%= bank.link %>" target="_blank">Сайт банка</a>
                    </div>
                </div>
                <div class="MortgageBanklist_inner-wrapper">
                    <div class="MortgageBanklist_inner">
                        <div class="MortgageBanklist_inner-row _column">
                            <div class="MortgageBanklist_inner-cell">
                                <span class="MortgageBanklist_inner-cell_key">Процентная ставка</span>
                                <span class="MortgageBanklist_inner-cell_val">от <%= bank.rate %>%</span>
                            </div>
                            <div class="MortgageBanklist_inner-cell">
                                <span class="MortgageBanklist_inner-cell_key">Первый взнос</span>
                                <span class="MortgageBanklist_inner-cell_val">от <%= bank.fee %>%</span>
                            </div>
                            <div class="MortgageBanklist_inner-cell">
                                <span class="MortgageBanklist_inner-cell_key">Срок кредита</span>
                                <span class="MortgageBanklist_inner-cell_val">от <%= bank.range %> лет</span>
                            </div>
                        </div>
                        <div class="MortgageBanklist_inner-row _single">
                            <p>&laquo;Альфа-банк&raquo;&nbsp;&mdash; это универсальный банк, выполняющий все основные виды банковских операций. По&nbsp;размеру активов и&nbsp;уставного капитала банк стабильно входит в&nbsp;50&nbsp;крупнейших российских банков.</p>
                        </div>

                        <% if (bank.objects) { %>
                        <div class="MortgageBanklist_inner-row">
                            <div class="MortgageBanklist_inner-row_tlt">Список ЖК</div>
                            <ul class="MortgageBanklist_obj-list">
                                <% _.forEach(bank.objects, function(obj){ %>
                                    <li class="MortgageBanklist_obj-list_item">
                                        <a href="<%= obj.link %>" class="MortgageBanklist_obj-list_link" target="_blank"><%= obj.name %></a>
                                    </li>
                                <%}); %>
                            </ul>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>
        <%}); %>
    </div>
</div>
