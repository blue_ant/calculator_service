{
    let $paymentCalcCont = $("#paymentCalcContainer");
    let $summCalcCont = $("#summCalcContainer");

    new Switch(".Switch", {
        onChange: ( /*isChecked*/ ) => {
            $paymentCalcCont.add($summCalcCont).toggle();
        }
    });
}

{
    let open = '_open';

    $(document).on('click', '[data-bank-toggle]', function() {
        let $row     = $(this);
        let $wrapper = $row.closest('.MortgageBanklist_wrapper');
        let $inner   = $row.next('.MortgageBanklist_inner-wrapper');

        if ($wrapper.hasClass(open)) {
            $wrapper.removeClass(open);
            $inner.stop().animate({
                height: 0
            });

            return;
        }

        $wrapper.addClass(open);
        $inner.stop().animate({
            height: $inner.get(0).scrollHeight
        });
    });
}

// START: Summ calculator
let summBanklistBuilder = new MortgageBanklist({
    mountEl: "#summCalcBanklistContainer",
    tpl: `
    /*=require includes/chunks/MortgageBanklist-summ.tpl*/
`,
});

let summCalcFlatsBuilder = new FlatsList({
    mountEl: "#summCalcFlatsContainer",
});

let summCalc = new MortgageSummCalc(".MortgageCalc-summ", {
    banksListBuilder: summBanklistBuilder,
    flatListBuilder: summCalcFlatsBuilder,
});
// END: Summ calculator

// START: Payment calculator
let paymentBanklistBuilder = new MortgageBanklist({
    mountEl: "#paymentCalcBanklistContainer",
    tpl: `
    /*=require includes/chunks/MortgageBanklist.tpl*/
`,
});

let paymentCalcFlatsBuilder = new FlatsList({
    mountEl: "#paymentCalcFlatsContainer",
});

let paymentCalc = new MortgagePaymentCalc(".MortgageCalc-payment", {
    banksListBuilder: paymentBanklistBuilder,
    flatListBuilder: paymentCalcFlatsBuilder,
});
// END: Payment calculator
